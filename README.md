# WebGL Game of Life

## Building

Ensure that you have Git and Node.js (version >= 11.0) installed.

Clone a copy of the repo:

`git clone https://gitlab.com/JMicheli/webgl-game-of-life.git`

Install dependencies:

`npm i`

Build and run application:

`npm run start`

## Controls

### Mouse

<kbd>LMB</kbd> - Place cells

<kbd>Ctrl</kbd> + <kbd>LMB</kbd> - Remove cells

### Keys

<kbd>space</kbd> - Pause/resume simulation

<kbd>c</kbd> - Clear cells

<kbd>r</kbd> - Randomize cells

© Joseph W. Micheli 2021, all rights reserved. See `License.txt` for further information.
