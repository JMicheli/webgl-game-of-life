module.exports = {
  "env": {
    "browser": true,
    "es2021": true
  },
  "extends": [
    "plugin:vue/essential",
    "google"
  ],
  "parserOptions": {
    "ecmaVersion": 13,
    "parser": "@typescript-eslint/parser",
    "sourceType": "module"
  },
  "plugins": [
    "vue",
    "@typescript-eslint"
  ],
  "rules": {
    "linebreak-style": ["off"],
    "eol-last": ["warn"],
    "indent": ["warn", 2],
    "max-len": ["error", 100],
    "no-trailing-spaces": ["warn"],
    "no-multi-spaces": ["warn"],
    "spaced-comment": ["warn", "always"],
    "quotes": ["warn", "double", { "avoidEscape": true, "allowTemplateLiterals": true }],
    "new-cap": ["off"],
    "brace-style": ["warn", "1tbs", { "allowSingleLine": true }],
    "block-spacing": ["warn", "always"],
    "template-curly-spacing": ["warn", "always"],
    "object-curly-spacing": ["warn", "always"],
    "comma-dangle": ["warn", "never"],
    "require-jsdoc": ["warn"],
    "valid-jsdoc": ["warn"]
  }
};
